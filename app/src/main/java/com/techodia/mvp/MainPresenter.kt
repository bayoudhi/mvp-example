package com.techodia.mvp


import com.orhanobut.logger.Logger
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MainPresenter(internal var view: MainContract.View) : MainContract.Presenter {

    internal var disposable: CompositeDisposable = CompositeDisposable()
    internal val RUNNING = "RUNNING"
    internal val NOT_RUNNING = "NOT_RUNNING"
    internal var state: String = RUNNING

    init {
        disposable.add(io.reactivex.Observable.interval(0, 1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t ->
                    run {
                        if(state==RUNNING){
                            view.updateResult(t.toString())
                            Logger.w(disposable.toString())
                        }else{
                            
                        }

                    }
                }))

    }

    override fun plus(a: Int, b: Int): Int = a + b

    override fun plusRx(a: Int, b: Int): Single<Int> = Single.fromCallable {
        Logger.w("Welcome im running the plusRx")
        a + b
    }

    override fun foix2(a: Int): Single<String> = Single.fromCallable { "result:" + a * a }

    override fun minus(a: Int, b: Int): Int = a - b

    override fun times(a: Int, b: Int): Int = a * b

    override fun divide(a: Int, b: Int): Int = a / b

    override fun onClickStop() {
        if (state == RUNNING) {
            state = NOT_RUNNING
        } else {
            state = RUNNING
        }
    }

    override fun onClickMagic(a: String, b: String) {
        if (view.verifInput()) {
            disposable.add(plusRx(Integer.parseInt(a), Integer.parseInt(b))
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { result, error ->
                        run {
                            if (error != null) {
                                error.printStackTrace()

                            } else {
                                view.updateResult("${result?.toString()}")
                            }
                        }
                    })
        } else {
            view.toast("Erroooor")
        }
    }
}
