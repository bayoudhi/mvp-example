package com.techodia.mvp


import io.reactivex.Single

interface MainContract {

    interface View {
        fun toast(message: String)

        fun updateResult(result: String)

        fun verifInput(): Boolean
    }

    interface Presenter {
        fun plus(a: Int, b: Int): Int

        fun plusRx(a: Int, b: Int): Single<Int>

        fun minus(a: Int, b: Int): Int

        fun times(a: Int, b: Int): Int

        fun divide(a: Int, b: Int): Int

        fun foix2(a: Int): Single<String>

        fun onClickMagic(a: String, b: String)

        fun onClickStop()

    }


}
