package com.techodia.mvp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), MainContract.View {

    internal var number1EditText: EditText?=null
    internal var number2EditText: EditText?=null
    internal var resultTextView: TextView?=null
    private var magicButton: Button? = null
    private var stopButton: Button? = null

    private var presenter: MainPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenter(this)

        number1EditText = findViewById(R.id.number1) as EditText
        number2EditText = findViewById(R.id.number2) as EditText
        resultTextView = findViewById(R.id.result) as TextView
        magicButton = findViewById(R.id.magic) as Button
        stopButton = findViewById(R.id.stop) as Button

        magicButton?.setOnClickListener({ presenter?.onClickMagic(number1EditText?.text.toString(), number2EditText?.text.toString()) })
        stopButton?.setOnClickListener { presenter?.onClickStop() }
    }


    override fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun updateResult(result: String) {
        resultTextView?.text = result
    }

    override fun verifInput(): Boolean {
        if (number1EditText?.text.toString().isNotEmpty() && number2EditText?.text.toString().isNotEmpty()) return true
        return false
    }

}
